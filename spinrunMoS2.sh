size=512
moments=1024
random=8
ebot=-5.5
etop=5.5
for i in {0..49}
	do
		python3 tmds_spin.py ${ebot} ${etop} ${size} ${moments} ${random}
		mv MoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}.h5  MoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5
		./KITEx MoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5
	       ./KITE-tools MoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.h5 --OpCondDC -F -5.5 5.5 11535 -E 11535 -N MoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.dat
	       mv dos.dat DOSMoS2SpinHallSize${size}x${size}Moments${moments}RandomV${random}Run${i}.dat
	done 
