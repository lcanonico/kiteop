import h5py
import numpy as np
file_name = 'haldaneOAM.h5'
f = h5py.File(file_name, 'r+')     # open the file

# List all groups
print('All groups')
for key in f.keys():  # Names of the groups in HDF5 file.
    print(key)
print()
print('Boundaries')
group = f['Boundaries']
print(np.array(group))
print('DIM')
group = f['DIM']
print(np.array(group))
print('Divisions')
group = f['Divisions']
print(np.array(group))
print('NOrbitals')
group = f['NOrbitals']
print(np.array(group))
print('Calculation')
group = f['Calculation']
for key in group.keys():
    print(key)
sub=group['Opconductivity_dc']
for key in sub.keys():
    print(key)
print('gamma')
print(np.array(sub['Gammaxy']))
'''
# Get the HDF5 group
group = f['Hamiltonian']
print(group)
# Checkout what keys are inside that group.
print('Single group')
for key in group.keys():
    print(key)
#print()
#if you want to modify other quantity, check de list and change the subgroup below
# Get the HDF5 subgroup
subgroup = group['d']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['Hoppings']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['NHoppings']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['StructuralDisorder']
print(subgroup)
dat = np.array(subgroup)
print(dat)
subgroup = group['Vacancy']
print(subgroup)
dat = np.array(subgroup)
subgroup = group['Disorder']
print(subgroup)
dat = np.array(subgroup)
print(dat)
'''



'''
new_value = 70
data = subgroup['Temperature']  # load the data
data[...] = new_value  # assign new values to data
f.close()  # close the file

# To confirm the changes were properly made and saved:

f1 = h5py.File(file_name, 'r')
print(np.allclose(f1['Calculation/AMP_conductivity_dc/Temperature'].value, new_value))
'''
