"""       
        ##############################################################################      
        #                        KITE | Pre-Release version 0.1                      #      
        #                                                                            #      
        #                        Kite home: quantum-kite.com                         #           
        #                                                                            #      
        #  Developed by: Simao M. Joao, Joao V. Lopes, Tatiana G. Rappoport,         #       
        #  Misa Andelkovic, Lucian Covaci, Aires Ferreira, 2018                      #      
        #                                                                            #      
        ##############################################################################      
"""
""" DOS and DC conductivity of the Haldane model

    Lattice : Honeycomb lattice;
    Disorder : Disorder class Uniform at different sublattices;
    Configuration : size of the system 256x256, with domain decomposition (nx=ny=1), periodic boundary conditions,
                    double precision, automatic scaling;
    Calculation : DOS and conductivity_dc (xy);
"""
import numpy as np
import kiteOM as kite
import pybinding as pb
from math import sqrt


def haldane():
    """Return the lattice specification for Haldane model"""

    a = 0.24595   # [nm] unit cell length
    a_cc = 0.142  # [nm] carbon-carbon distance

    t=-1
    t2 = t/10
    m=.0
    # create a lattice with 2 primitive vectors
    lat = pb.Lattice(
        a1=[a, 0],
        a2=[a/2, a/2 * sqrt(3)]
    )

    lat.add_sublattices(
        # name and position
        ('A-up', [0, -a_cc/2],m),
        ('A-down', [0,  -a_cc/2],m),
        ('B-up', [0, a_cc/2],-m),
        ('B-down', [0,  a_cc/2],-m)
    )


    lat.add_hoppings(
        # inside the main cell
        ([0,  0], 'A-up', 'B-up', t),
        # between neighboring cells
        ([1, -1], 'A-up', 'B-up', t),
        ([0, -1], 'A-up', 'B-up', t),
        ([1, 0], 'A-up', 'A-up', t2 * 1j),
        ([0, -1], 'A-up', 'A-up', t2 * 1j),
        ([-1, 1], 'A-up', 'A-up', t2 * 1j),
        ([1, 0], 'B-up', 'B-up', t2 * -1j),
        ([0, -1], 'B-up', 'B-up', t2 * -1j),
        ([-1, 1], 'B-up', 'B-up', t2 * -1j),
    #####Spin Down Sector
        # inside the main cell
        ([0,  0], 'A-down', 'B-down', t),
        # between neighboring cells
        ([1, -1], 'A-down', 'B-down', t),
        ([0, -1], 'A-down', 'B-down', t),
        ([1, 0], 'A-down', 'A-down', -t2 * 1j),
        ([0, -1], 'A-down', 'A-down', -t2 * 1j),
        ([-1, 1], 'A-down', 'A-down', -t2 * 1j),
        ([1, 0], 'B-down', 'B-down', t2 * 1j),
        ([0, -1], 'B-down', 'B-down', t2 * 1j),
        ([-1, 1], 'B-down', 'B-down', t2 * 1j)




    )

    return lat


def haldaneOM():
    """Return the lattice specification for Haldane model"""

    a = 0.24595   # [nm] unit cell length
    a_cc = 0.142  # [nm] carbon-carbon distance

    t=-1
    t2 = t/10
    m=0
    # create a lattice with 2 primitive vectors
    lat = pb.Lattice(
        a1=[a, 0],
        a2=[a/2, a/2 * sqrt(3)]
    )

    lat.add_sublattices(
        # name and position
        ('A-up', [0, -a_cc/2],0.5),
        ('A-down', [0,  -a_cc/2],-0.5),
        ('B-up', [0, a_cc/2],0.5),
        ('B-down', [0,  a_cc/2],-0.5)
    )

    return lat








# make a haldane lattice
lattice = haldane()
orbital = haldaneOM()
# add Uniformly distributed disorder
disorder = kite.Disorder(lattice)
disorder.add_disorder('A-up', 'Uniform', +0.0, 0.6)
disorder.add_disorder('B-up', 'Uniform', +0.0, 0.6)
disorder.add_disorder('A-down', 'Uniform', +0.0, 0.6)
disorder.add_disorder('B-down', 'Uniform', +0.0, 0.6)
# number of decomposition parts in each direction of matrix.
# This divides the lattice into various sections, each of which is calculated in parallel
nx = ny = 4
# number of unit cells in each direction.
lx = ly = 256
# make config object which caries info about
# - the number of decomposition parts [nx, ny],
# - lengths of structure [lx, ly]
# - boundary conditions, setting True as periodic boundary conditions, and False elsewise,
# - info if the exported hopping and onsite data should be complex,
# - info of the precision of the exported hopping and onsite data, 0 - float, 1 - double, and 2 - long double.
# - scaling, if None it's automatic, if present select spectrum_bound=[e_min, e_max]
configuration = kite.Configuration(divisions=[nx, ny], length=[lx, ly], boundaries=[True, True],
                                   is_complex=True, precision=0,spectrum_range=[-4.0,4.0])
calculation = kite.Calculation(configuration)
# require the calculation of DOS and conductivity_dc
calculation.dos(num_points=6000, num_moments=512, num_random=1, num_disorder=1)
calculation.Opconductivity_dc(num_points=6000, num_moments=512, num_random=1, num_disorder=1,direction='xy', temperature=100)
# configure the *.h5 file
kite.config_system(lattice, configuration, calculation,OM=orbital, filename='haldaneOM.h5',disorder=disorder)

