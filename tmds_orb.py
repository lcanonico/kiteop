"""       
        ##############################################################################      
        #                        KITE | Pre-Release version 0.1                      #      
        #                                                                            #      
        #                        Kite home: quantum-kite.com                         #           
        #                                                                            #      
        #  Developed by: Simao M. Joao, Joao V. Lopes, Tatiana G. Rappoport,         #       
        #  Misa Andelkovic, Lucian Covaci, Aires Ferreira, 2018                      #      
        #                                                                            #      
        ##############################################################################      
"""
""" Onsite disorder

    Lattice : Monolayer graphene;
    Disorder : Disorder class Deterministic and Uniform at different sublattices,
    Configuration : size of the system 512x512, without domain decomposition (nx=ny=1), periodic boundary conditions,
                    double precision, manual scaling;
    Calculation : dos;
"""

import kiteOM as kite
#from pybinding.repository import graphene
import pybinding as pb
import group6_tmdsoc as tmd
#from pybinding.repository import group6_tmd as tmd
import matplotlib.pyplot as plt
# load graphene lattice and structural_disorder
import argparse
import sys

parser = argparse.ArgumentParser(description='para incluir')
if(len(parser.parse_known_args()[1])!=5):
    print("Error: Incorrect Number of parameters... The parameters of the program are ebot, etop, size, moments and R")
    sys.exit()
else:
    pass
#convert the parameters
ebot=float(parser.parse_known_args()[1][0])
etop=float(parser.parse_known_args()[1][1])
size=int(parser.parse_known_args()[1][2])
moments=int(parser.parse_known_args()[1][3])
R=int(parser.parse_known_args()[1][4])


lattice = tmd.monolayer_3band_soc("MoS2")
orbital = tmd.monolayer_3band_lz("MoS2")
'''
#lattice.plot()
model = pb.Model(lattice, pb.translational_symmetry())
solver = pb.solver.lapack(model)
k_points = model.lattice.brillouin_zone()
gamma = [0, 0]
k = k_points[0]
m = (k_points[0] + k_points[1]) / 2
plt.figure(figsize=(6.7, 2.3))
plt.subplot(121, title="MoS2 3-band model band structure")
bands = solver.calc_bands(gamma, k, m, gamma)
bands.plot(point_labels=[r"$\Gamma$", "K", "M", r"$\Gamma$"])
#plt.show()
'''
# add Disorder
disorder = kite.Disorder(lattice)
#disorder.add_disorder('B', 'Deterministic', -1.0)
#disorder.add_disorder("Mo", 'Uniform',0, 0.01)
# number of decomposition parts in each direction of matrix.
# This divides the lattice into various sections, each of which is calculated in parallel
nx = ny = 2
# number of unit cells in each direction.
lx = ly = size
# make config object which caries info about
# - the number of decomposition parts [nx, ny],
# - lengths of structure [lx, ly]
# - boundary conditions, setting True as periodic boundary conditions, and False elsewise,
# - info if the exported hopping and onsite data should be complex,
# - info of the precision of the exported hopping and onsite data, 0 - float, 1 - double, and 2 - long double.
# - scaling, if None it's automatic, if present select spectrum_bound=[e_min, e_max]
configuration = kite.Configuration(divisions=[nx, ny], length=[lx, ly], boundaries=[True, True],
                                   is_complex=True,spectrum_range=[ebot, etop], precision=1)
# require the calculation of DOS
calculation = kite.Calculation(configuration)
calculation.dos(num_points=6535, num_moments=moments, num_random=R, num_disorder=1)
calculation.Opconductivity_dc(num_points=6535, num_moments=moments, num_random=R, num_disorder=1,direction='xy', temperature=100)
# configure the *.h5 file
name="MoS2"+"OrbitalHall"+"Size"+str(size)+"x"+str(size)+"Moments"+str(moments)+"RandomV"+str(R)+".h5"
kite.config_system(lattice, configuration, calculation,OM=orbital, filename=name)

